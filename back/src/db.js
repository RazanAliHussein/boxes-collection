import sqlite from "sqlite";
import SQL from "sql-template-strings";

// const test = async () => {
//   const db = await sqlite.open("./db.sqlite");

//   await db.run(
//     `CREATE TABLE boxes (boxes_id INTEGER PRIMARY KEY AUTOINCREMENT, length INTEGER NOT NULL, width INTEGER NOT NULL,height INTEGER NOT NULL,material TEXT NOT NULL,lock TEXT NOT NULL);`
//   );

//   const stmt = await db.prepare(
//     SQL`INSERT INTO boxes (length, width,height,material,lock) VALUES (?, ?, ?, ?, ?)`
//   );
//   let i = 0;
//   while (i < 10) {
//     await stmt.run(`${i}`, `${i}`, `${i}`, "wood", "lock");
//     i++;
//   }

//   await stmt.finalize();

//   const rows = await db.all(
//     "SELECT boxes_id AS id, length,width,height ,material, lock FROM boxes"
//   );
//   rows.forEach(({ id, length, width, height, material, lock }) =>
//     console.log(
//       `[id:${id}] - ${length} - ${width},${height},${material},${lock}`
//     )
//   );
// };

// export default { test };
const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");

  /**
   * retrieves the contacts from the database
   */
  const getBoxesList = async () => {
   // let returnString=""
    const rows = await db.all(
      "SELECT boxes_id AS id, length,width,height ,material, lock FROM boxes"
    );
  // rows.forEach( ({ id, length, width, height, material, lock }) => returnString+=`[id:${id}] - ${length} - ${width},${height},${material},${lock}` )
    return rows;
  };
  //CRUD
  const createBoxes = async props => {
    if (
      !props ||
      !props.length ||
      !props.height ||
      !props.width ||
      !props.material ||
      !props.lock
    ) {
      throw new Error(`you must provide a name and an email`);
    }

    const { length, width, height, material, lock } = props;
    try {
      const result = await db.run(
        SQL`INSERT INTO boxes (length, width, height, material, lock) VALUES (${length}, ${width} ,${height},${material},${lock})`
      );
      const id = result.stmt.lastID;
      return id;
    } catch (e) {
      throw new Error(`couldn't insert this combination: ` + e.message);
    }
  };
  //delete
  const deleteBoxes = async id => {
    try {
      const result = await db.run(SQL`DELETE FROM boxes WHERE boxes_id= ${id}`);
      if (result.stmt.changes === 0) {
        throw new Error(`box "${id}" does not exist`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't delete the box "${id}": ` + e.message);
    }
  };
  //update
  const updateBox = async (id, props) => {
    const { length, width, height, material, lock } = props;
    const result = await db.run(
      SQL`UPDATE boxes SET lock=${lock} WHERE boxes_id = ${id}`
    );
    if (result.stmt.changes === 0) {
      return false;
    }
    return true;
  };
  const getBox = async id => {
    try {
      const boxesList = await db.all(
        SQL`SELECT boxes_id AS id, length,width,height ,material, lock FROM boxes WHERE boxes_id = ${id}`
      );
      const box = boxesList[0];
      if (!box) {
        throw new Error(`box ${id} not found`);
      }
      return bursh;
    } catch (e) {
      throw new Error(`couldn't get the box ${id}: ` + e.message);
    }
  };
  const getBoxList = async orderBy => {
    try {
      let statement = `SELECT boxes_id AS id, length,width,height ,material, lock FROM boxes`;
      switch (orderBy) {
        case "material":
          statement += ` ORDER BY material`;
          break;
        case "lock":
          statement += ` ORDER BY lock`;
          break;
        case "height":
          statement += ` ORDER BY height`;
          break;
        case "width":
          statement += ` ORDER BY width`;
          break;
        case "length":
          statement += ` ORDER BY length`;
          break;
        default:
          break;
      }
      const rows = await db.all(statement);
      if (!rows.length) {
        throw new Error(`no rows found`);
      }
      return rows;
    } catch (e) {
      throw new Error(`couldn't retrieve contacts: ` + e.message);
    }
  };
  //CRUD
  const controller = {
    getBoxesList,
    createBoxes,
    deleteBoxes,
    updateBox,
    getBox,
    getBoxList
  };

  return controller;
};

export default initializeDatabase;
