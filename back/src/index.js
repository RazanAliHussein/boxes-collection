// import { createServer } from 'http'


// const whenRequestReceived = (request /* the request sent by the client*/, response /* the response we use to answer*/) => {

//   response.writeHead(200, { 'Content-type': `text/plain` });
 
//   response.write(`Hello`);

//   response.end( );
// }

// const server = createServer(whenRequestReceived)

// server.listen(8080, ()=>{console.log('ok, listening')});
// import app from './app'
// import db from './db'
// app.get( '/', (req, res) => res.send("ok") );

// app.listen( 8080, () => console.log('server listening on port 8080') )
// db.test()
// import app from './app'
// import initializeDatabase from './db'

// const start = async () => {
//   const controller = await initializeDatabase()
//   const boxes_list = await controller.getBoxesList()
//   console.log(boxes_list)
//   //app.get('/',(req,res)=>res.send("ok"));
//   //app.listen(8080, ()=>console.log('server listening on port 8080'))
// }

// start();
import app from './app'
import initializeDatabase from './db'
import { authenticateUser, logout, isLoggedIn } from './auth'

const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get('/boxes/list', async (req, res) => {
    const boxes_list = await controller.getBoxesList()
    res.json(boxes_list)
  })
  //Auth
  app.get('/login', authenticateUser)
  
  app.get('/logout', logout)
  
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.name
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })

  // CREATE
  app.get("/boxes/new", async (req, res, next) => {
    try {
      const { length, width, height, material, lock } = req.query;
      const result = await controller.createBoxes({ length, width, height, material, lock });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  // READ
  app.get("/box/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const brush = await controller.getBox(id);
      res.json({ success: true, result: brush });
    } catch (e) {
      next(e);
    }
  });
  // DELETE
  app.get("/box/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteBoxes(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  // UPDATE
  app.get("/box/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { length, width, height, material, lock } = req.query;
      const result = await controller.updateBox(id, { length, width, height, material, lock });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  // LIST
  app.get("/boxes/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const box = await controller.getBoxList(order);
      res.json({ success: true, result: box });
    } catch (e) {
      next(e);
    }
  });

  app.use((err, req, res, next) => {
    try {
      console.error(err);
      const message = err.message;
      res.status(500).json({ success: false, message });
    } catch (e) {
      next(e);
    }
  });

  app.listen(8080, () => console.log("server listening on port 8080"));

}
start();
