import React, { Component } from "react";
import { withRouter, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import BoxesList from './BoxesList.js';
class App extends Component {
  state = {
    boxes_list: [],
    error_message: "",
    length: "",
    width: "",
    height: "",
    material: "",
    lock: ""
  };
  // async componentDidMount(){
  //   try{
  //     const response = await fetch('//localhost:8080/boxes/list')
  //     const answer = await response.json()
  //     if(answer.success){
  //       const boxes_list = answer.result
  //       this.setState({boxes_list})
  //     }else{
  //       const error_message = answer.message
  //       this.setState({error_message})
  //     }
  //   }catch(err){
  //     this.setState({error_message:err.message})
  //   }
  // }
 ///link
 renderHomePage = () => {
  const { boxes_list } = this.state;
  return <BoxesList boxes_list={boxes_list} />;
}
renderContactPage = ({ match }) => {
  const id = match.params.id;
  // find the contact:
  const box = this.state.boxes_list.find(box => box.id == id);
  // we use double equality because our ids are numbers, and the id provided by the router is a string
  if (!box) {
    return <div>{id} not found</div>;
  }
  return (
   <div>
      <span>
              {box.height}-{box.width}-{box.length}-{box.material}-{box.lock}
            </span>
   </div>
  );
}
renderProfilePage = () => {
  return <div>profile page</div>;
}
renderCreateForm = () => {
  return  <form className="third" onSubmit={this.onSubmit}>
  <input
    type="text"
    placeholder="height"
    onChange={evt => this.setState({ height: evt.target.value })}
    value={this.state.height}
  />
  <input
    type="text"
    placeholder="width"
    onChange={evt => this.setState({ width: evt.target.value })}
    value={this.state.width}
  />
   <input
    type="text"
    placeholder="length"
    onChange={evt => this.setState({ length: evt.target.value })}
    value={this.state.length}
  />
   <input
    type="text"
    placeholder="material"
    onChange={evt => this.setState({ material: evt.target.value })}
    value={this.state.material}
  />
   <input
    type="text"
    placeholder="lock"
    onChange={evt => this.setState({ lock: evt.target.value })}
    value={this.state.lock}
  />
  <div>
    <input type="submit" value="ok" />
    <input type="reset" value="cancel" className="button" />
  </div>
</form>
}
renderContent() {
  if (this.state.isLoading) {
    return <p>loading...</p>;
  }
  return (
    <Switch>
      <Route path="/" exact render={this.renderHomePage} />
      <Route path="/box/:id" render={this.renderContactPage} />
      <Route path="/profile" render={this.renderProfilePage} />
      <Route path="/create" render={this.renderCreateForm} />
      <Route render={()=><div>not found!</div>}/>
    </Switch>
  );
}

 ///link end
  getBoxesList = async () => {
    try {
      const response = await fetch("//localhost:8080/boxes/list");
      const data = await response.json();
      this.setState({ boxes_list: data });
      console.log({ data });
    } catch (err) {
      console.log(err);
    }
  };
  //get one box
  getBox = async id => {
    // check if we already have the contact
    const previous_contact = this.state.boxes_list.find(
      contact => contact.id === id
    );
    if (previous_contact) {
      return; // do nothing, no need to reload a contact we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/box/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const contact = answer.result;
        const boxes_list = [...this.state.boxes_list, contact];
        this.setState({ boxes_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  //get one box end
  //delete box
  deleteBox = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/box/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const boxes_list = this.state.boxes_list.filter(
          contact => contact.id !== id
        );
        this.setState({ boxes_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  //delete box end
  //Edit
  updateBox = async id =>{

  }
  //Edit end
  //create
  createBox = async props => {
    try {
      if (!props || !(props.length && props.height && props.width && props.material && props.lock)) {
        throw new Error(
          `you need both name and email properties to create a contact`
        );
      }
      //{length}, ${width} ,${height},${material},${lock}
      const { length, width,height,material,lock } = props;
      const response = await fetch(
        `http://localhost:8080/boxes/new/?length=${length}&width=${width}&height=${height}&material=${material}&lock=${lock}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const contact =  { length, width,height,material,lock } ;
        const boxes_list = [...this.state.boxes_list, contact];
        this.setState({boxes_list});
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  //create end
  //get
  getBoxList = async order => {
    try {
      const response = await fetch(
        `http://localhost:8080/boxes/list?order=${order}`
      );
      const answer = await response.json();
      if (answer.success) {
        const boxes_list = answer.result;
        this.setState({ boxes_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  //get end
  componentDidMount() {
    this.getBoxesList();
  }
  onSubmit = () => {
    // extract name and email from state
    const  { length, width,height,material,lock }  = this.state
    // create the contact from mail and email
    this.createBox( { length, width,height,material,lock } )
    // empty name and email so the text input fields are reset
    this.setState({length:"", width:"",height:"",material:"",lock:""})
  }

  render() {
    return (
      <div className="App">
        {/* {this.state.boxes_list.map(box => (
          <div key={box.id}>
            <span>
              {box.height}-{box.width}-{box.length}-{box.material}-{box.lock}
            </span>
            <button onClick={() => this.deleteBox(box.id)} className="warning">x</button>
  

          </div>
        ))} */}

          <div>
              <Link to="/">Home</Link> | 
              <Link to="/profile">profile</Link> | 
              <Link to="/create">create</Link> 
          </div>
          {this.renderContent()}


      </div>
    );
  }
}
export default withRouter(App)
