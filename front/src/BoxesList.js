import React from "react";
import { Transition } from "react-spring"; // you can remove this from App.js
import { Link } from "react-router-dom";

const BoxesList = ({ boxes_list }) => (
  <Transition
    items={boxes_list}
    keys={contact => contact.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { box => style => (
      <div style={style}>
        <Link to={"/box/"+box.id}>{box.material}</Link>
      </div>
    )}
  </Transition>
);

export default BoxesList
